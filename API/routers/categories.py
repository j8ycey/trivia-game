from fastapi import APIRouter, Depends
from pydantic import BaseModel

from db import CategoryQueries

router = APIRouter()

class CategoryIn(BaseModel):
    title: str

class CategoryOut(BaseModel):
    id: int
    title: str
    canon: bool

class CategoriesOut(BaseModel):
    categories: list[CategoryOut]


@router.get("/api/categories", response_model=CategoriesOut)
def get_categories(queries: CategoryQueries = Depends(), page: int = None):
    if page:
        return {"categories": queries.get_all_categories(offset=page*100)}
    return {"categories": queries.get_all_categories()}

@router.post("/api/categories", response_model=CategoryOut)
def create_category(category: CategoryIn, queries: CategoryQueries = Depends()):
    return queries.create_category(category)

@router.put("/api/categories/{cat_id}", response_model=CategoryOut)
def update_category(cat_id: int, category: CategoryIn, queries: CategoryQueries = Depends()):
    return queries.update_category(category, cat_id)

@router.delete("/api/categories/{cat_id}", response_model=bool)
def delete_category(cat_id: int, queries: CategoryQueries = Depends()):
    return queries.delete_category(cat_id)