from fastapi import FastAPI, Depends
from pydantic import BaseModel
from typing import Union

from routers import categories


app = FastAPI()

app.include_router(categories.router)